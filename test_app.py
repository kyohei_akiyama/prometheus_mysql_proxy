# -*- coding: utf-8 -*-

import os
import re

from nose.tools import ok_
from webtest import TestApp
from app import wsgi_app

RULES = [
    (re.compile(r'^mysql_uptime_seconds \d+$'), 1,),
    (re.compile(r'^mysql_connection_total \d+$'), 1,),
    (re.compile(r'^mysql_connection_limit \d+$'), 1,),
    (re.compile(r'^mysql_variable_innodb_log_file_size \d+$'), 1,),
    (re.compile(r'^mysql_connection_used_max \d+$'), 1,),
    (re.compile(r'^mysql_slow_queries \d+$'), 1,),
    (re.compile(r'^mysql_queries\{.*\} \d+$'), 4,),
    (re.compile(r'^mysql_thread_activities\{.*\} \d+$'), 4,),
    (re.compile(r'^mysql_heavy_queries\{.*\} \d+$'), 9,),
    (re.compile(r'^mysql_handler_activities\{.*\} \d+$'), 7,),
    (re.compile(r'^mysql_open_files\{.*\} \d+$'), 2,),
    (re.compile(r'^mysql_open_tables\{.*\} \d+$'), 2,),
    (re.compile(r'^mysql_tmp_tables\{.*\} \d+$'), 3,),
    (re.compile(r'^mysql_innodb_status\{.*\} \d+$'), 48,),
    (re.compile(r'^mysql_engine_innodb_status\{.*\} \d+$'), 25,)
]


def test_get_db_metrics():
    url = '/metrics?host=%s&port=%s&user=%s&password=%s' % (
        os.environ['DB_HOST'], os.environ['DB_PORT'],
        os.environ['DB_USER'], os.environ['DB_PASS']
    )
    app = TestApp(wsgi_app)
    res = app.get(url)

    ok_(res.status_int == 200)
    stats = res.body.decode('utf-8').split('\n')
    for rule, line_num in RULES:
        ok_(len([s for s in stats if rule.match(s)]) == line_num)
