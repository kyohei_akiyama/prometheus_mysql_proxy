#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup
from setuptools import find_packages


def _requirements():
    return [
        name.rstrip()
        for name in open('requirements.txt').readlines()
    ]


def main():
    setup(
        name='prometheus_mysql_proxy',
        version='0.0.1',
        packages=find_packages(),
        install_requires=_requirements(),
    )


if __name__ == '__main__':
    main()
