# -*- coding: utf-8 -*-

import os
import sys
import traceback
import re
import MySQLdb
from bottle import Bottle, run, request, response

from collectd_mysql import (
    fetch_mysql_status,
    fetch_mysql_master_stats,
    fetch_mysql_slave_stats,
    fetch_mysql_variables,
    fetch_innodb_stats
)

NAME_PREFIX = os.getenv('NAME_PREFIX', default='mysql')


def get_mysql_conn(conn_args):
    return MySQLdb.connect(**conn_args)


def get_up(up=True):
    if up:
        return ['%s_up 1' % (NAME_PREFIX)]
    else:
        return ['%s_up 0' % (NAME_PREFIX)]


def get_mysql_master_stats(conn):
    stats = fetch_mysql_master_stats(conn)
    ret = [
        '%s_master_status{name="%s"} %s' % (NAME_PREFIX, label, value)
        for label, value in stats.items()
    ]
    return ret


def get_slave_stats(conn):
    stats = fetch_mysql_slave_stats(conn)
    ret = [
        '%s_slave_status{name="%s"} %s' % (NAME_PREFIX, label, value)
        for label, value in stats.items()
    ]
    return ret


def get_global_stats(conn):
    ret = []
    stats = fetch_mysql_status(conn)
    variables = fetch_mysql_variables(conn)

    # Uptime
    ret.append('%s_uptime_seconds %s' % (NAME_PREFIX, stats['Uptime']))

    # Total connections
    ret.append('%s_connection_total %s' % (NAME_PREFIX, stats['Connections']))

    # Connection limits
    ret.append('%s_connection_limit %s' %
               (NAME_PREFIX, variables['max_connections']))

    # InnoDB size
    ret.append('%s_variable_innodb_log_file_size %s' %
               (NAME_PREFIX, variables['innodb_log_file_size']))

    # Max used connections
    ret.append('%s_connection_used_max %s' %
               (NAME_PREFIX, stats['Max_used_connections']))

    # Slow queries
    ret.append('%s_slow_queries %s' % (NAME_PREFIX, stats['Slow_queries']))

    # Query counter
    r = re.compile(r'^Com_(select|insert|update|delete)$')
    for name, value in stats.items():
        match = r.findall(name)
        if match:
            ret.append('%s_queries{query="%s"} %s' %
                       (NAME_PREFIX, match[0], value))

    # Thread activity
    r = re.compile(r'^Threads_(.*)')
    for name, value in stats.items():
        match = r.findall(name)
        if match:
            ret.append('%s_thread_activities{name="%s"} %s' %
                       (NAME_PREFIX, match[0], value))

    # Heavy queries
    r = re.compile(r'^((?:Select|Sort)_.*)')
    for name, value in stats.items():
        match = r.findall(name)
        if match:
            ret.append('%s_heavy_queries{name="%s"} %s' %
                       (NAME_PREFIX, match[0].lower(), value))

    # Handler activity
    r = re.compile(r'^Handler_read_(.*)')
    for name, value in stats.items():
        match = r.findall(name)
        if match:
            ret.append('%s_handler_activities{name="%s"} %s' %
                       (NAME_PREFIX, match[0], value))

    # Open files
    r = re.compile(r'^(Open(?:ed)?)_files')
    for name, value in stats.items():
        match = r.findall(name)
        if match:
            ret.append('%s_open_files{name="%s"} %s' %
                       (NAME_PREFIX, match[0].lower(), value))

    # Open tables.
    r = re.compile(r'^(Open(?:ed)?)_tables')
    for name, value in stats.items():
        match = r.findall(name)
        if match:
            ret.append('%s_open_tables{name="%s"} %s' %
                       (NAME_PREFIX, match[0].lower(), value))

    # Temporary table
    r = re.compile(r'^Created_tmp_(.*)')
    for name, value in stats.items():
        match = r.findall(name)
        if match:
            ret.append('%s_tmp_tables{name="%s"} %s' %
                       (NAME_PREFIX, match[0].lower(), value))

    # InnoDB stats
    r = re.compile(r'^Innodb_(.*)')
    for name, value in stats.items():
        match = r.findall(name)
        if match and str(value).isdigit():
            ret.append('%s_innodb_status{name="%s"} %s' %
                       (NAME_PREFIX, match[0].lower(), value))
    return ret


def get_engine_innodb_stats(conn):
    stats = fetch_innodb_stats(conn)
    ret = [
        '%s_engine_innodb_status{name="%s"} %s' % (NAME_PREFIX, label, value)
        for label, value in stats.items()
    ]
    return ret


wsgi_app = Bottle()


@wsgi_app.get('/metrics')
def get_db_metrics():
    try:
        conn_args = {
            'host': request.query['host'],
            'port': int(request.query['port']),
            'user': request.query['user'],
            'passwd': request.query.get('password', default=''),
            'connect_timeout': 1
        }
        stats = []
        try:
            conn = get_mysql_conn(conn_args)
            stats += get_up(up=True)
            stats += get_mysql_master_stats(conn)
            stats += get_slave_stats(conn)
            stats += get_global_stats(conn)
            stats += get_engine_innodb_stats(conn)
            conn.close()
        except Exception as e:
            # 何らかのエラーが起きた場合はダウン扱いとする
            stats += get_up(up=False)

        response.content_type = 'text/plain'
        return '\n'.join(stats) + '\n'
    except Exception as e:
        response.status = 500
        st = traceback.format_exc()
        sys.stderr.write(st)
        return st


if __name__ == '__main__':
    run(wsgi_app, host='localhost', port=8080)
