## Installing dependent packages
```shell
pip install bottle mysqlclient
```

or

```shell
pip install -r requirements.txt
```

## Running
```shell
python app.py
```

## Running with Gunicorn
```shell
gunicorn -b 0.0.0.0:8000 app:wsgi_app
```

## Usage
```shell
$ curl 'http://127.0.0.1:8000/metrics?host=127.0.0.1&port=3306&user=dbuser&password=dbpass'
mysql_master_status{name="binary_log_space"} 4325829
mysql_connection_total 8000
mysql_slow_queries 388
mysql_queries{query="select"} 295
mysql_queries{query="insert"} 114822
mysql_queries{query="update"} 2
mysql_queries{query="delete"} 1
mysql_thread_activities{name="cached"} 3
mysql_thread_activities{name="created"} 6
mysql_thread_activities{name="connected"} 3
mysql_thread_activities{name="running"} 2
mysql_heavy_queries{name="sort_rows"} 968
mysql_heavy_queries{name="sort_scan"} 2
mysql_heavy_queries{name="sort_range"} 0
mysql_heavy_queries{name="sort_merge_passes"} 0
mysql_heavy_queries{name="select_range"} 0
mysql_heavy_queries{name="select_full_join"} 0
mysql_heavy_queries{name="select_range_check"} 0
mysql_heavy_queries{name="select_scan"} 16096
mysql_heavy_queries{name="select_full_range_join"} 0
mysql_handler_activities{name="next"} 26167
mysql_handler_activities{name="key"} 9057
mysql_handler_activities{name="last"} 1
mysql_handler_activities{name="rnd_next"} 7089242
mysql_handler_activities{name="first"} 8084
mysql_handler_activities{name="prev"} 999
mysql_handler_activities{name="rnd"} 968
mysql_open_files{name="opened"} 24117
mysql_open_files{name="open"} 22
mysql_open_tables{name="open"} 168
mysql_open_tables{name="opened"} 175
mysql_innodb_status{name="buffer_pool_pages_dirty"} 0
mysql_innodb_status{name="row_lock_time_max"} 0
mysql_innodb_status{name="buffer_pool_bytes_dirty"} 0
mysql_innodb_status{name="rows_updated"} 2
mysql_innodb_status{name="buffer_pool_write_requests"} 1046201
mysql_innodb_status{name="pages_read"} 527
mysql_innodb_status{name="buffer_pool_read_requests"} 2036479
mysql_innodb_status{name="row_lock_current_waits"} 0
mysql_innodb_status{name="data_fsyncs"} 497
mysql_innodb_status{name="data_reads"} 567
mysql_innodb_status{name="buffer_pool_wait_free"} 0
mysql_innodb_status{name="log_write_requests"} 77918
mysql_innodb_status{name="os_log_fsyncs"} 215
mysql_innodb_status{name="buffer_pool_read_ahead_rnd"} 0
mysql_innodb_status{name="buffer_pool_pages_free"} 64241
mysql_innodb_status{name="page_size"} 16384
mysql_innodb_status{name="pages_written"} 25990
mysql_innodb_status{name="rows_inserted"} 2939091
mysql_innodb_status{name="truncated_status_writes"} 0
mysql_innodb_status{name="buffer_pool_resize_status"} 
mysql_innodb_status{name="data_read"} 8720896
mysql_innodb_status{name="row_lock_time"} 0
mysql_innodb_status{name="data_pending_writes"} 0
mysql_innodb_status{name="data_writes"} 26285
mysql_innodb_status{name="buffer_pool_read_ahead"} 0
mysql_innodb_status{name="data_written"} 457211904
mysql_innodb_status{name="buffer_pool_pages_data"} 1283
mysql_innodb_status{name="row_lock_waits"} 0
mysql_innodb_status{name="available_undo_logs"} 128
mysql_innodb_status{name="num_open_files"} 31
mysql_innodb_status{name="buffer_pool_reads"} 528
mysql_innodb_status{name="data_pending_fsyncs"} 0
mysql_innodb_status{name="buffer_pool_pages_total"} 65528
mysql_innodb_status{name="dblwr_writes"} 0
mysql_innodb_status{name="os_log_pending_writes"} 0
mysql_innodb_status{name="log_waits"} 0
mysql_innodb_status{name="buffer_pool_pages_misc"} 4
mysql_innodb_status{name="row_lock_time_avg"} 0
mysql_innodb_status{name="buffer_pool_pages_flushed"} 25990
mysql_innodb_status{name="rows_deleted"} 24975
mysql_innodb_status{name="rows_read"} 2858313
mysql_innodb_status{name="buffer_pool_read_ahead_evicted"} 0
mysql_innodb_status{name="log_writes"} 186
mysql_innodb_status{name="data_pending_reads"} 0
mysql_innodb_status{name="os_log_written"} 31376896
mysql_innodb_status{name="pages_created"} 756
mysql_innodb_status{name="dblwr_pages_written"} 0
mysql_innodb_status{name="buffer_pool_bytes_data"} 21020672
mysql_innodb_status{name="os_log_pending_fsyncs"} 0
```

## Test
```shell
DB_HOST=127.0.0.1 DB_PORT=3306 DB_USER=dbuser DB_PASS=dbuser nosetests -sv test_app.py
```

or

```shell
DB_HOST=127.0.0.1 DB_PORT=3306 DB_USER=dbuser DB_PASS=dbuser tox
```
